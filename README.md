# GitHub Navigator

In order to run the project, make sure you're in a Python 3.5 environment then
run:

    pip install -r requirements.txt

That's it. Now just run:

    python manage.py runserver

And the app should be accessible at http://127.0.0.1:8000/
