import requests
from operator import itemgetter


class RateLimitExceeded(Exception):
    pass


def get_latest_commit(repo):
    return requests.get(repo['commits_url'][:-6]).json()[0]


def get_repos(search_term, user, password, limit=5):
    if user and password:
        requests.get(
            'https://api.github.com/arielpontes',
            auth=(user, password)
        )
    response = requests.get(
        'https://api.github.com/search/repositories?q=%s' % search_term)
    if response.status_code == 403:
        raise RateLimitExceeded
    items = response.json()['items']
    newest = sorted(items, key=itemgetter('created_at'))[:limit]
    repositories = []
    for full_repo in newest:
        latest_commit = get_latest_commit(full_repo)
        min_repo = {
            'name': full_repo['name'],
            'created_at': full_repo['created_at'],
            'owner': {
                'url': full_repo['owner']['url'],
                'avatar_url': full_repo['owner']['avatar_url'],
                'login': full_repo['owner']['login']
            },
            'latest_commit': {
                'sha': latest_commit['sha'],
                'message': latest_commit['commit']['message'],
                'author_name': latest_commit['commit']['author']['name']
            }
        }
        repositories.append(min_repo)
    return repositories
