from django.shortcuts import render

from core.github_consumer import get_repos, RateLimitExceeded


def index(request):
    search_term = request.GET.get('search_term')
    user = request.GET.get('user')
    password = request.GET.get('pass')

    repositories = []

    if search_term:
        try:
            repositories = get_repos(search_term, user, password)
        except RateLimitExceeded:
            return render(request, 'core/rate_limit_exceeded.html')

    context = {'search_term': search_term, 'repositories': repositories}
    return render(request, 'core/index.html', context)
