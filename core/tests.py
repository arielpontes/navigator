from unittest.mock import patch

from django.test import TestCase, Client


MOCK_REPOS = [
    {
        'created_at': '2008-12-08T08:28:57Z',
        'latest_commit': {
            'author_name': 'raymond.hettinger',
            'message': (
                "Issue 5171: itertools.product docstring missing "
                "'repeat' argument\n\n\n\ngit-svn-id: "
                "http://svn.python.org/projects/python/trunk@69466 "
                "6015fed2-1504-0410-9fe1-9d1591cc4771"
            ),
            'sha': '715a6e5035bb21ac49382772076ec4c630d6e960'
        },
        'name': 'python',
        'owner': {
            'avatar_url': 'https://avatars.githubusercontent.com/u/39016?v=3',
            'login': 'python-git',
            'url': 'https://api.github.com/users/python-git'
        }
    },
    {
        'created_at': '2011-09-02T08:51:40Z',
        'latest_commit': {
            'author_name': 'Parita Pooj',
            'message': (
                'Merge pull request #6 from sks147/master\n\nAdded new '
                'random subroutine which can extract text from ms-word file'
            ),
            'sha': '20382603660c68c68c321aba8548652df0c29e83'
        },
        'name': 'python',
        'owner': {
            'avatar_url': 'https://avatars.githubusercontent.com/u/900519?v=3',
            'login': 'parita',
            'url': 'https://api.github.com/users/parita'
        }
    }
]


class GitHubNavigatorTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_no_search(self):
        with patch('requests.get') as requests_get:
            response = self.client.get('/navigator/')
            requests_get.assert_not_called()
        self.assertContains(response, "Your search didn't match any results.")

    def test_no_results(self):
        with patch(
                'core.views.get_repos', return_value=[]) as get_repos:
            response = self.client.get(
                '/navigator/', {'search_term': 'bad_search'})
            get_repos.assert_called_once_with('bad_search', None, None)
        self.assertContains(response, "Your search didn't match any results.")

    def test_rate_limit_exceeded(self):
        class mock_response(object):
            status_code = 403
        with patch(
                'requests.get',
                return_value=mock_response) as requests_get:
            response = self.client.get('/navigator/', {'search_term': 'foo'})
            requests_get.assert_called_once_with(
                'https://api.github.com/search/repositories?q=foo')
        self.assertContains(response, "You've reached your limit")

    def test_successful_search(self):
        with patch(
                'core.views.get_repos', return_value=MOCK_REPOS) as get_repos:
            response = self.client.get(
                '/navigator/', {'search_term': 'python'})
            get_repos.assert_called_once_with('python', None, None)
        self.assertContains(
            response, '<h3 class="panel-title">1. python</h3>')
        self.assertContains(
            response,
            """
            <div class="media">
              <div class="media-left">
                <a href="https://api.github.com/users/python-git">
                  <img src="https://avatars.githubusercontent.com/u/39016?v=3" \
                  alt="avatar" class="thumbnail" height="42" width="42">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">python-git</h4>
              </div>
            </div>
            """,
            html=True
        )
        self.assertContains(
            response, '<h3 class="panel-title">2. python</h3>')
        self.assertContains(
            response,
            """
            <div class="media">
              <div class="media-left">
                <a href="https://api.github.com/users/parita">
                  <img src="https://avatars.githubusercontent.com/u/900519?v=3" \
                  alt="avatar" class="thumbnail" height="42" width="42">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">parita</h4>
              </div>
            </div>
            """,
            html=True
        )
